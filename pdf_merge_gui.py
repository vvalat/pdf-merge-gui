import os
import uuid

import tkinter as tk
import tkinter.filedialog as fd
import tkinter.messagebox as msg
from PyPDF2 import PdfFileMerger, PdfFileWriter, PdfFileReader, utils


class PDFApp(tk.Tk):
    def __init__(self, *args, **kwargs):
        tk.Tk.__init__(self, *args, **kwargs)
        self.wm_title("PDF Merge GUI")
        self._frame = None
        self.directory = None

    def load_directory(self):
        self.directory = fd.askdirectory() or os.path.curdir
        self.file_selector.load_files([f for f in os.listdir(self.directory) if f.endswith(".pdf")])


class MainComponent(tk.Frame):
    def __init__(self, master, controller):
        tk.Frame.__init__(self, master)
        self.master = master
        self.controller = controller
        self.pack()

        tk.Label(
            self,
            text="Order the PDF files in this box by selecting one file and moving it up or down",
        ).grid(row=0)

        box_container = tk.Frame(self)
        box_container.grid(row=1, pady=5)

        self.file_box = tk.Listbox(box_container, height=10, width=50, borderwidth=1)
        self.file_box.pack(side=tk.LEFT, fill=tk.Y)

        scrollbar = tk.Scrollbar(box_container, orient="vertical")
        scrollbar.pack(side=tk.RIGHT, fill=tk.Y)

        self.load_files([f for f in os.listdir(os.path.curdir) if f.endswith(".pdf")])

        scrollbar.config(command=self.file_box.yview)
        self.file_box.config(yscrollcommand=scrollbar.set)

        btn_container = tk.Frame(self)
        btn_container.grid(row=2)

        tk.Button(btn_container, text="▲", command=lambda: self.move("up")).grid(row=0, column=0)
        tk.Button(btn_container, text="▼", command=lambda: self.move("down")).grid(row=1, column=0)

        password_container = tk.Frame(self)
        password_container.grid(row=3, pady=25)
        self.checkbox = tk.BooleanVar()
        self.checkbox.set(False)
        tk.Checkbutton(password_container, variable=self.checkbox).grid(row=0, column=0)
        tk.Label(password_container, text="Password protection").grid(row=0, column=1)
        self.password = tk.StringVar()
        tk.Entry(password_container, show="*", width=20, textvariable=self.password).grid(
            row=0, column=2, padx=10
        )

        tk.Button(self, text="Merge PDF", command=self.generate_pdf).grid(row=4, pady=15)

    def load_files(self, file_list):
        self.file_box.delete(0, tk.END)
        self.file_box.insert(0, *file_list)

    def move(self, direction):
        idx = self.file_box.curselection()
        if not idx:
            return
        old_pos = idx[0]
        new_pos = old_pos + (-1 if direction == "up" else 1)
        list_length = len(self.file_box.get(0, tk.END))
        if new_pos < 0 or new_pos > list_length - 1:
            return
        switched = self.file_box.get(new_pos)
        self.file_box.delete(new_pos)
        self.file_box.insert(old_pos, switched)

    def generate_pdf(self):
        pdf_order = self.file_box.get(0, tk.END)
        if len(pdf_order) < 2:
            return msg.showerror("Error", "You must have at least two PDF files to process")

        merger = PdfFileMerger(strict=False)  # Allow some errors in the xref table if fixable

        for pdf_file in pdf_order:
            try:
                # Cannot use a context manager to close the file due to a PyPDF2 bug
                current = open(os.path.join(self.master.directory, pdf_file), "rb")
                merger.append(fileobj=current)
            except (IOError, utils.PdfReadError) as exc:
                # Permission errors, file being deleted before being read, encrypted data...
                print(exc)
                return msg.showerror("Error", f"{pdf_file} could not be read. Aborting merge.")

        try:
            file_name = f"merged_pdf_{uuid.uuid4()}.pdf"
            with open(file_name, "wb") as output_file:
                merger.write(output_file)
        except Exception:  # Don't know what will happen...
            return msg.showerror("Error", "Could not write to output file.")

        if self.checkbox.get() and (password := self.password.get()):
            # Should not have to write the PDF *then* encrypt it *then* delete but it will do here
            encrypted = PdfFileWriter()
            encrypted.encrypt(password)
            with open(file_name, "rb") as pdf_file:
                pdf = PdfFileReader(pdf_file)
                # Should not be done this way but cloneDocumentFromReader is buggy
                for page_number in range(pdf.getNumPages()):
                    encrypted.addPage(pdf.getPage(page_number))
                old_file_name = str(file_name)
                file_name = f"merged_pdf_{uuid.uuid4()}.pdf"
                encrypted.write(open(file_name, "wb"))
            os.remove(old_file_name)

        return msg.showinfo("Success", f"File saved under the name {file_name}")


def generate_menu(tk_app):
    menu_bar = tk.Menu(tk_app)
    file_menu = tk.Menu(menu_bar, tearoff=0)
    file_menu.add_command(label="Load", command=tk_app.load_directory)
    file_menu.add_separator()
    file_menu.add_command(label="Quit", command=tk_app.quit)
    menu_bar.add_cascade(label="File", menu=file_menu)
    return menu_bar


if __name__ == "__main__":
    app = PDFApp()
    app.resizable(False, False)  # One way to handle resizing issues
    app.config(menu=generate_menu(app))
    app.tk.call(
        "wm", "iconphoto", app._w, tk.PhotoImage(file=os.path.realpath("./pdf_merge_gui.png"))
    )
    setattr(app, "file_selector", MainComponent(app, app))
    app.geometry("800x400")
    app.mainloop()
