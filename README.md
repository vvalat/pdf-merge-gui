# PDF Merge GUI

Self-imposed challenge (less than a day) to create a tkinter GUI on some code to merge PDF files into one with the possibility of ordering files and setting a password for protection. I needed it for housing rental scouting...

- Took 4 hours to get everything working without the password feature
- Spent one hour trying to debug the PDF lib before resorting to a dirty hack
- Spent 30 minutes trying different scenarios
- Another 30 minutes of cleaning up the code

Updated for the move to a new repository. Fixed two bugs (bad reference to source directory,
handling of some PDF with bad xref table)

Possible next steps:
- Doing something right when handling the encrypted case
- Handling window resizing (unlikely)
- Allowing naming merged file
